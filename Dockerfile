FROM alpine:3.16.2
RUN apk --update add --no-cache \
    make \
    git \
    shadow \
    cargo \
    python3 \
    py3-pip \
    gcc \
    && groupadd -g 1000 ansible \
    && useradd --no-log-init -m -u 1000 -g 1000 ansible
USER ansible
RUN python3 -m pip install --no-cache-dir --upgrade --user ansible

ENV PATH "$PATH:/home/ansible/.local/bin"
ENTRYPOINT ["ansible"]
